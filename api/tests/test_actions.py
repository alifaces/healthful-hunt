from fastapi.testclient import TestClient
from main import app
from queries.actions import ActionsQueries
from authenticator import authenticator
from datetime import date


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "fakeuser"}


class FakeActionsQueries:
    def get_all(self, goal_id: str, date_completed: date, user_id: str):
        return [
            {
                "id": goal_id,
                "user_id": user_id,
                "date_completed": date_completed,
            }
        ]


def test_get_all_actions_by_date():
    app.dependency_overrides[ActionsQueries] = FakeActionsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    goal_id = "643f233c137d82cf52f520cd"
    date_completed = "2023-04-25"
    res = client.get(f"/api/goals/{goal_id}/actions/{date_completed}")
    data = res.json()
    print("data:", data)
    assert res.status_code == 200
    assert "actions" in data
    assert type(data["actions"]) == list
    assert data["actions"][0]["user_id"] == "fakeuser"
    app.dependency_overrides = {}
