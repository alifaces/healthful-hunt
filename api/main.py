from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import goals, accounts, actions
from authenticator import authenticator

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(accounts.router, tags=["Accounts"])
app.include_router(authenticator.router, tags=["Accounts Auth"])
app.include_router(goals.router, tags=["Goals"])
app.include_router(actions.router, tags=["Actions"])
