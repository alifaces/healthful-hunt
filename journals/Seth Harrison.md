# 3/26/23

Afternoon- Worked together to set up endpoints for apis. Did the 'create', 'update', and 'delete' goal apis.

# 3/30/23

Worked together to make a function that gets a list of all actions associated with a particular goal, regardless of date

# 2pm-5pm

Worked to get the date field as a date object in the actions.

# 4/18/23

Fixed update goal so that it starts with the current (before updated) information in the form and actually updates the goal when you submit

# 4/25/23

Created modal for Signup and worked to build the decrement action button, and try to get it functional

# 4/28/23

Remove comments from models and merge 'seth' branch with main
