# 3/27/2023

Documented api-design and created journal files for everyone.

# 3/29/2023

Added PUT method for goals

# 4/7/2023

worked as a group to create the update goals method but it is not working

# 4/14/2023

got the signup working

# 4/19/2023

worked as a group to troubleshoot and create the login modal

# 4/22/2023

fixed update, login, signup modal and styled the goals list page

# 4/28/2023

removed commented out code and comments, worked as a group to do merge requests
