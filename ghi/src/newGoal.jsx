import React, { useState } from "react";
import { useCreateGoalMutation } from "./services/goals";
import { ButtonComp } from "./components/ButtonComp";
import { FormInputText } from "./components/FormInputText";
import { useNavigate } from "react-router-dom";
import { icons } from "./components/iconOptions";

const NewGoal = () => {
  const [name, setName] = useState("");
  const [frequency, setFrequency] = useState("");
  const [icon_url, setIcon_url] = useState("");
  const [start_date, setStartDate] = useState(new Date());
  const [createGoal] = useCreateGoalMutation();
  const navigate = useNavigate();

  return (
    <form
      className="py-3 form-container"
      onSubmit={(e) => {
        e.preventDefault();
        createGoal({ name, frequency, icon_url, start_date }).then((resp) => {
          if (resp.data) {
            navigate("/goals/list");
          }
        });
      }}
    >
      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setName(e.target.value);
          }}
          value={name}
          label={"Input the habit you want to track"}
          type={"text"}
        />
      </div>
      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setFrequency(e.target.value);
          }}
          value={frequency}
          label={"Input the number of times daily"}
          type={"text"}
        />
      </div>

      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setStartDate(e.target.value);
          }}
          value={start_date}
          label={"Start date"}
          type={"date"}
        />
      </div>
      <div className="mb-3">
        <select
          id="icon_url"
          className="font-bold text-black rounded-md  form-select bg-sky-500"
          value={icon_url}
          onChange={(e) => setIcon_url(e.target.value)}
        >
          <option value="">Choose an icon</option>
          {icons.map((icon) => (
            <option key={icon.name} value={icon.svg}>
              {icon.name}
            </option>
          ))}
        </select>
      </div>

      <ButtonComp title="Submit" type="submit" />
    </form>
  );
};

export default NewGoal;
