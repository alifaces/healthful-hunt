import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const authApi = createApi({
  reducerPath: "authApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}`,
    credentials: "include",
  }),
  endpoints: (builder) => ({
    getAccount: builder.query({
      query: () => "/token",
      transformResponse: (response) => response?.account,
      providesTags: ["Account"],
    }),
    login: builder.mutation({
      query: (body) => {
        const formData = new FormData();
        formData.append("username", body.username);
        formData.append("password", body.password);
        return {
          url: "/token",
          method: "POST",
          body: formData,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account", { type: "Goals", id: "LIST" }],
    }),
    signup: builder.mutation({
      query: (body) => {
        return {
          url: "/api/accounts",
          method: "POST",
          body,
        };
      },
      invalidatesTags: ["Account", "Goals"],
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
      }),
      invalidatesTags: ["Account", { type: "Goals", id: "LIST" }],
    }),
  }),
});

export const {
  useGetAccountQuery,
  useLogoutMutation,
  useLoginMutation,
  useSignupMutation,
} = authApi;

export const goalsApi = createApi({
  reducerPath: "goalsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/goals/`,
    credentials: "include",
  }),
  endpoints: (builder) => ({
    deleteGoal: builder.mutation({
      query: (id) => ({
        url: id,
        method: "DELETE",
      }),
      invalidatesTags: (result, error, { id }) => [{ type: "Goals", id }],
    }),
    createGoal: builder.mutation({
      query: (body) => {
        console.log("body:", body);
        return {
          url: "",
          method: "POST",
          body,
        };
      },
      invalidatesTags: [{ type: "Goals", id: "LIST" }],
    }),
    getGoals: builder.query({
      query: () => "",
      transformResponse: (response) => response.goals,
      providesTags: (result) => {
        const tags = [{ type: "Goals", id: "LIST" }];
        if (!result) return tags;
        return [...result.map(({ id }) => ({ type: "Goals", id })), ...tags];
      },
    }),
    getGoalById: builder.query({
      query: (id) => ({
        url: `${id}`,
        method: "GET",
      }),
    }),
    updateGoal: builder.mutation({
      query: (body) => {
        console.log("Body Update:", body);
        return {
          url: body.id,
          method: "PUT",
          body: body,
        };
      },
      invalidatesTags: (result, error, { id }) => [{ type: "Goals", id }],
    }),
  }),
});

export const {
  useCreateGoalMutation,
  useDeleteGoalMutation,
  useGetGoalsQuery,
  useGetGoalByIdQuery,
  useUpdateGoalMutation,
} = goalsApi;

export const actionsApi = createApi({
  reducerPath: "actionsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/`,
    credentials: "include",
  }),
  endpoints: (builder) => ({
    deleteActions: builder.mutation({
      query: (actionId) => ({
        url: `actions/${actionId}`,
        method: "DELETE",
      }),
      invalidatesTags: (result, error, { id }) => [{ type: "Actions", id }],
    }),
    createActions: builder.mutation({
      query: (body) => {
        return {
          url: `goals/${body.goal_id}/actions`,
          method: "POST",
          body: { date_completed: body.dateCompleted },
        };
      },
      invalidatesTags: [{ type: "Actions", id: "LIST" }],
    }),
    getActions: builder.query({
      query: (params) => {
        return {
          url: `goals/${params.goal_id}/actions/${params.date_completed}`,
          method: "GET",
        };
      },
      transformResponse: (response) => response.actions,
      providesTags: (result, error) => {
        const { goalId, dateCompleted } = result?.meta?.arg ?? {};
        const tags = [{ type: "Actions", id: "LIST", goalId, dateCompleted }];
        if (!result) return tags;
        return [
          ...result.map(({ id }) => ({
            type: "Actions",
            id,
            goalId,
            dateCompleted,
          })),
          ...tags,
        ];
      },
    }),
  }),
});

export const {
  useGetActionsQuery,
  useDeleteActionsMutation,
  useCreateActionsMutation,
} = actionsApi;
