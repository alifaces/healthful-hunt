import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Login from "./Login";
import Signup from "./Signup";
import Logout from "./Logout";
import NewGoal from "./newGoal";
import Goals from "./ListGoals";
import UpdateGoal from "./updateGoal";
import { useGetAccountQuery } from "./services/goals";
import Nav from "./Nav";
import { LoginModal } from "./components/LoginModal";
import { useSelector } from "react-redux";
import { useEffect, useRef } from "react";
import autoAnimate from "@formkit/auto-animate";

function App() {
  const { data: account } = useGetAccountQuery();
  const modalComp = useSelector((state) => state.modalSlice);
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  const parent = useRef(account);

  useEffect(() => {
    parent.current && autoAnimate(parent.current);
  }, [parent]);

  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div ref={parent}>{modalComp.isActive && <LoginModal />}</div>
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="login" element={<Login />} />

        <Route path="goals">
          <Route path="new" element={<NewGoal />} />
          <Route path="list" element={<Goals />} />
          <Route path=":id" element={<UpdateGoal />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
