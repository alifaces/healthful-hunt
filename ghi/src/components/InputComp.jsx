import React from 'react'

export const InputComp = ({labelTitle}) => {
  return (
    <div className='p-3 font-bold text-purple-500 bg-blue-200 rounded-full w-[100px] text-center ease-in duration-200 hover:bg-blue-300 cursor-pointer'>{labelTitle}InputComp</div>
  )
}
