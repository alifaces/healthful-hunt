import React from 'react'

export const ButtonComp = ({title, type, onClickFunc}) => {
  return (
    <button type={type} onClick={onClickFunc} className='p-3 font-bold text-black bg-sky-500 rounded-full w-[100px] text-center ease-in duration-200 hover:bg-red-500 cursor-pointer'>{title}</button>
  )
}
