import React from "react";
import "./mainPage.css";
import svgIcon from "./assets/download.svg";

function MainPage() {
  return (
    <div className="text-center">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="text-6xl font-bold">Healthful Hunt</h1>
        <div className="mx-auto col-lg-6">
          <br />
          <p className="mb-4 text-xl italic lead font">
            The premier solution for building healthy habits!
          </p>
        </div>
      </div>
      <div
        className="background-image"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          height: "90vh",
          border: "3px solid black",
          padding: "10px",
          width: "80vw",
          borderRadius: "20px",
          overflowX: "auto",
          marginBottom: "20px",
          overflow: "hidden",
          maxWidth: "100%",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: "100%",
            height: "10%",
            marginTop: "20px",
          }}
        />
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            padding: "10px",
            width: "100%",
            height: "60%",
            borderRadius: "20px",
            marginBottom: "20px",
          }}
        >
          <a href="/healthful-hunt/signup">
            <button
              style={{
                width: "350px",
                height: "125px",
                margin: "0 10px",
                border: "none",
                backgroundColor: "transparent",
                color: "white",
                borderRadius: "10px",
                fontSize: "18px",
                fontWeight: "bold",
                background: `url(${svgIcon}) no-repeat center`,
                backgroundSize: "125% 125%",
                paddingTop: "20px",
              }}
            >
              GET STARTED
            </button>
          </a>
        </div>
      </div>
      <div className="border-2 border-black rounded-lg p-25 ">
        <div className="flex justify-center">
          <div className="flex flex-col items-center justify-center mx-auto">
            <div className="flex items-center justify-center w-2/4 h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:h-auto">
              <p className="px-4 font-bold text-center">HOW IT WORKS</p>
            </div>
            <div className="flex items-center justify-center h-64 m-2 bg-gray-200 border-2 border-black rounded-lg w-4/4 md:h-auto">
              <p className="px-4 italic font-bold text-center">
                BEHAVIOURISTS HAVE IDENTIFIED 3 SIMPLE STEPS TO ACHIEVING ANY
                GOAL:
              </p>
            </div>
          </div>
        </div>
        <div className="flex justify-center w-full">
          <div className="flex items-center justify-center w-full h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:w-1/3 xl:w-1/7 md:h-auto">
            <p className="px-4 italic font-bold text-center">1. SET A GOAL</p>
          </div>
          <div className="flex items-center justify-center w-full h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:w-1/3 xl:w-1/7 md:h-auto">
            <p className="px-4 italic font-bold text-center">
              2. ACTUALLY DO IT
            </p>
          </div>
          <div className="flex items-center justify-center w-full h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:w-1/3 xl:w-1/7 md:h-auto">
            <p className="px-4 italic font-bold text-center ">
              3. TRACK YOUR PROGRESS
            </p>
          </div>
        </div>
        <div className="flex justify-between w-full">
          <div className="flex items-center justify-center w-full h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:w-1/3 xl:w-1/7 md:h-auto">
            <p className="px-4 italic font-bold text-center">
              Select what habits you would like to develop or quit and start
              your journey
            </p>
          </div>
          <div className="flex items-center justify-center w-full h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:w-1/3 xl:w-1/7 md:h-auto">
            <p className="px-4 italic font-bold text-center">
              We help keep you accountable by sending you reminders at intervals
              that you decide, to ensure that you never forget!
            </p>
          </div>
          <div className="flex items-center justify-center w-full h-64 m-2 bg-gray-200 border-2 border-black rounded-lg md:w-1/3 xl:w-1/7 md:h-auto">
            <p className="px-4 italic font-bold text-center">
              See your daily, weekly or monthly progress as you work towards
              meeting your goals
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
