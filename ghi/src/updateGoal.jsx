import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { useGetGoalByIdQuery } from "./services/goals";
import { icons } from "./components/iconOptions";
import { ButtonComp } from "./components/ButtonComp";

import {
  handleNameChange,
  handleFrequencyChange,
  handleIconUrlChange,
  handleStartDateChange,
} from "./features/goal/updateGoalSlice";
import { selectGoal } from "..//src/features/goal/selectGoalSlice";
import { useUpdateGoalMutation } from "./services/goals";
import { useNavigate } from "react-router-dom";
import { FormInputText } from "./components/FormInputText";

function UpdateGoal() {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { data: goalData } = useGetGoalByIdQuery(id);
  const formFields = useSelector((state) => state.updateGoal);
  const navigate = useNavigate();
  const [updateGoalMutation] = useUpdateGoalMutation();
  const [icon_url, setIcon_url] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault();
    updateGoalMutation({
      id: id,

      name: formFields.name,
      frequency: formFields.frequency,
      icon_url: formFields.icon_url,
      start_date: formFields.start_date,
    }).then((resp) => {
      if (resp.data) {
        navigate("/goals/list");
      }
    });
  };
  React.useEffect(() => {
    if (goalData) {
      dispatch(selectGoal(goalData));
      dispatch(handleNameChange(goalData.name));
      dispatch(handleFrequencyChange(goalData.frequency));
      dispatch(handleIconUrlChange(goalData.icon_url));
      dispatch(handleStartDateChange(goalData.start_date));
    }
  }, [dispatch, goalData]);

  return (
    <div className="py-3 form-container">
      <form onSubmit={handleFormSubmit}>
        <br />
        <div>
          {" "}
          <div className="flex items-center">
            <FormInputText
              type="text"
              label={"Update goal name"}
              className="block w-full px-6 pt-6 pb-1 italic font-bold text-black rounded-md appearance-none text-md bg-neutral-700 focus:outline-none focus:ring-0 peer invalid:border-b-1"
              id="name-field"
              placeholder="Goal 1"
              tabIndex={1}
              value={formFields.name}
              onChange={(e) => dispatch(handleNameChange(e.target.value))}
            />
          </div>
        </div>
        <br />
        <FormInputText
          label={"Update the frequency"}
          type="int"
          className="block w-full px-6 pt-6 pb-1 italic font-bold text-black rounded-md appearance-none text-md bg-neutral-700 focus:outline-none focus:ring-0 peer invalid:border-b-1"
          id="frequency-field"
          placeholder="Frequency"
          tabIndex={1}
          value={formFields.frequency}
          onChange={(e) => dispatch(handleFrequencyChange(e.target.value))}
        />
        <br />
        <div className="form-group">
          <FormInputText
            label={"Update start date: "}
            type="date"
            className="italic font-bold text-black rounded-lg form-control"
            id="date-field"
            tabIndex={1}
            value={formFields.start_date}
            onChange={(e) => dispatch(handleStartDateChange(e.target.value))}
          />
        </div>
        <div className="mb-3">
          <select
            id="icon_url"
            className="font-bold text-black rounded-md  form-select bg-sky-500"
            value={icon_url}
            onChange={(e) => setIcon_url(e.target.value)}
          >
            <option value="">Update icon</option>
            {icons.map((icon) => (
              <option key={icon.name} value={icon.svg}>
                {icon.name}
              </option>
            ))}
          </select>
        </div>
        <ButtonComp title="Submit" type="submit" />
      </form>
    </div>
  );
}

export default UpdateGoal;
